import { useEffect, useState } from 'react';

export const useManageList = (url) => {
  const [list, setList] = useState([]);

  useEffect(() => {
    fetchList();
  }, []);

  const fetchList = async () => {
    const response = await fetch(url);
    const list = await response.json();
    setList(list);
  };

  const add = async (note) => {
    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(note),
      headers: { 'Content-Type': 'application/json' },
    });
    const newPost = await response.json();
    setList([...list, newPost]);
  };

  const edit = async (note) => {
    const response = await fetch(`${url}/${note.id}`, {
      method: 'PUT',
      body: JSON.stringify(note),
      headers: { 'Content-Type': 'application/json' },
    });
    const editedPost = await response.json();
    setList(list.map((el) => (el.id === editedPost.id ? editedPost : el)));
  };

  const remove = async (id) => {
    await fetch(`${url}/${id}`, {
      method: 'DELETE',
    });
    setList(list.filter((el) => el.id !== id));
  };

  return [list, add, edit, remove];
};
