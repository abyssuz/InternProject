export const useFetch = (callback) => {
  const fetching = async () => {
    await callback();
  };

  return [fetching];
};
