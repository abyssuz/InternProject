import React from 'react';
import OrderForm from './pages/OrderForm';
import AdminPanel from './pages/AdminPanel';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import './main.css';

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <OrderForm />
        </Route>
        <Route path="/admin">
          <AdminPanel />
        </Route>
        <Redirect to="/" />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
