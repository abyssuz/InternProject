import React, { useMemo, useState } from 'react';
import '../Components/OrderForm/OrderForm.css';
import NameInput from '../Components/OrderForm/ui/NameInput';
import EmailInput from '../Components/OrderForm/ui/EmailInput';
import ClocksizeRadio from '../Components/OrderForm/ui/ClocksizeSelect';
import CitySelect from '../Components/OrderForm/ui/CitySelect';
import DatePicker from '../Components/OrderForm/ui/DatePicker';
import TimeSelect from '../Components/OrderForm/ui/TimeSelect';
import RepairersList from '../Components/OrderForm/ui/RepairersList';
import { ORDER_URL } from '../assets/apiUrls';
import SubmitButton from '../Components/commonUI/SubmitButton';

const OrderForm = () => {
  const [orderName, setOrderName] = useState('');
  const [orderEmail, setOrderEmail] = useState('');
  const [orderOptions, setOrderOptions] = useState({
    clockSize: '',
    cityId: '',
    date: '',
    time: '',
  });
  const [selectedRepairerId, setSelectedRepairerId] = useState(null);

  const showRepairers = useMemo(() => {
    for (const prop in orderOptions) {
      if (orderOptions[prop] === '') {
        return false;
      }
    }
    return true;
  }, [orderOptions]);

  const onNameInputChange = (value) => {
    setOrderName(value);
  };

  const onEmailInputChange = (value) => {
    setOrderEmail(value);
  };

  const onOrderOptionsChange = (changes) => {
    setOrderOptions({ ...orderOptions, ...changes });
  };

  const onRepairerClick = (target, id) => {
    document
      .querySelector('.order-repairer.active')
      ?.classList.toggle('active');
    target.classList.toggle('active');
    setSelectedRepairerId(id);
  };

  const onOrderBtnClick = (e) => {
    e.preventDefault();

    const newOrder = {
      name: orderName,
      email: orderEmail,
      ...orderOptions,
      repairerId: selectedRepairerId,
    };

    fetch(ORDER_URL, {
      method: 'POST',
      body: JSON.stringify(newOrder),
      headers: { 'Content-Type': 'application/json' },
    });
  };

  return (
    <form className="order-form">
      <label htmlFor="order-name">Your name:</label>
      <NameInput
        nameInputValue={orderName}
        onNameInputChange={onNameInputChange}
      />

      <label htmlFor="order-email">Your email:</label>
      <EmailInput
        emailInputValue={orderEmail}
        onEmailInputChange={onEmailInputChange}
      />

      <div className="order-options-box">
        <label htmlFor="order-clock-size">Clock size:</label>
        <ClocksizeRadio onOrderOptionsChange={onOrderOptionsChange} />

        <label htmlFor="order-city">City:</label>
        <CitySelect onOrderOptionsChange={onOrderOptionsChange} />

        <label htmlFor="order-date">Date:</label>
        <DatePicker onOrderOptionsChange={onOrderOptionsChange} />

        <label htmlFor="order-time">Time:</label>
        <TimeSelect onOrderOptionsChange={onOrderOptionsChange} />
      </div>

      {showRepairers ? (
        <RepairersList
          cityId={orderOptions.cityId}
          onRepairerClick={onRepairerClick}
        />
      ) : (
        ''
      )}

      <SubmitButton value="Make order" onClick={onOrderBtnClick} />
    </form>
  );
};

export default OrderForm;
