import React from 'react';
import '../Components/AdminPanel/AdminPanel.css';
import AdminNav from '../Components/AdminPanel/ui/AdminNav';

const AdminPanel = () => {
  return <AdminNav />;
};

export default AdminPanel;
