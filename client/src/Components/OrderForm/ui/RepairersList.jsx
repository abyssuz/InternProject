import React, { useEffect, useState } from 'react';
import { REPAIRER_URL } from '../../../assets/apiUrls';

const RepairersList = ({ cityId, onRepairerClick }) => {
  const [list, setList] = useState([]);

  useEffect(() => {
    fetch(`${REPAIRER_URL}/${cityId}`)
      .then((resp) => resp.json())
      .then((data) => setList(data));
  }, [cityId]);

  return (
    <div>
      {list.map((item) => {
        return (
          <p
            className="order-repairer"
            onClick={(e) => onRepairerClick(e.target, item.id)}
            key={item.id}
          >{`Name: ${item.name}, rating: ${item.rating}`}</p>
        );
      })}
    </div>
  );
};

export default RepairersList;
