import React from 'react';

const EmailInput = ({ emailInputValue, onEmailInputChange }) => {
  return (
    <input
      className="order-email"
      type="email"
      value={emailInputValue}
      placeholder="example@example.com"
      onChange={(e) => onEmailInputChange(e.target.value)}
    ></input>
  );
};

export default EmailInput;
