import React from 'react';

const DatePicker = ({ onOrderOptionsChange }) => {
  return (
    <input
      className="order-date"
      type="date"
      onChange={(e) => onOrderOptionsChange({ date: e.target.value })}
    ></input>
  );
};

export default DatePicker;
