import React from 'react';

const NameInput = ({ nameInputValue, onNameInputChange }) => {
  return (
    <input
      className="order-name"
      type="text"
      value={nameInputValue}
      placeholder="Enter your name..."
      onChange={(e) => onNameInputChange(e.target.value)}
    ></input>
  );
};

export default NameInput;
