import React, { useState, useEffect } from 'react';
import { CITY_URL } from '../../../assets/apiUrls';

const CitySelect = ({ onOrderOptionsChange }) => {
  const [citiesList, setCitiesList] = useState([]);

  useEffect(() => {
    fetch(CITY_URL)
      .then((resp) => resp.json())
      .then((data) => setCitiesList(data));
  }, []);

  return (
    <select
      className="order-city"
      onChange={(e) => onOrderOptionsChange({ cityId: e.target.value })}
    >
      <option value="">Choose city</option>
      {citiesList.map((city) => (
        <option key={city.id} value={city.id}>
          {city.name}
        </option>
      ))}
    </select>
  );
};

export default CitySelect;
