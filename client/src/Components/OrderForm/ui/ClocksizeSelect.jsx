import React from 'react';

const ClocksizeRadio = ({ onOrderOptionsChange }) => {
  return (
    <div
      className="clock-size-select"
      onChange={(e) => onOrderOptionsChange({ clockSize: e.target.value })}
    >
      <div className="clocksize-radio">
	      <input id="radio-1" type="radio" name="clocksize" value="small"/>
	      <label htmlFor="radio-1">Small</label>
      </div>      
      <div className="clocksize-radio">
	      <input id="radio-2" type="radio" name="clocksize" value="medium" />
	      <label htmlFor="radio-2">Medium</label>
      </div>
      <div className="clocksize-radio">
	      <input id="radio-3" type="radio" name="clocksize" value="large" />
	      <label htmlFor="radio-3">Large</label>
      </div>
    </div>
  );
};

export default ClocksizeRadio;
