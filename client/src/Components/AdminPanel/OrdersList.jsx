import React, { useEffect, useState } from 'react';
import { useFetch } from '../../hooks/useFetch';
import { ORDER_URL } from '../../assets/apiUrls';

const OrdersList = () => {
  const [list, setList] = useState([]);
  const [fetchOrders] = useFetch(async () => {
    const response = await fetch(ORDER_URL);
    const orders = await response.json();
    setList(orders);
  });

  useEffect(() => {
    fetchOrders();
  }, []);

  return (
    <div>
      {list.map((el) => (
        <div className="list-item" key={el.id}>
          {el.clockSize},{el.city},{el.date},{el.time},{el.clientId}, {el.repairerId}
        </div>
      ))}
    </div>
  );
};

export default OrdersList;
