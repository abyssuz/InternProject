import React, { useEffect, useState } from 'react';
import { useFetch } from '../../hooks/useFetch';
import { CLIENT_URL } from '../../assets/apiUrls';

const CitiesList = () => {
  const [list, setList] = useState([]);
  const [fetchClients] = useFetch(async () => {
    const response = await fetch(CLIENT_URL);
    const clients = await response.json();
    setList(clients);
  });

  useEffect(() => {
    fetchClients();
  }, []);

  return (
    <div>
      {list.map((el) => (
        <div className="list-item" key={el.id}>
          {el.name}
        </div>
      ))}
    </div>
  );
};

export default CitiesList;
