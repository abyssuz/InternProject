import React, { useState } from 'react';

const LoginForm = () => {
  const [adminEmail, setAdminEmail] = useState('');
  const [adminPassword, setAdminPassword] = useState('');

  return (
    <form>
      <input
        type="email"
        value={adminEmail}
        placeholder="example@example.com"
        onChange={(e) => setAdminEmail(e.target.value)}
      ></input>
      <input
        type="password"
        value={adminPassword}
        placeholder="password..."
        onChange={(e) => setAdminPassword(e.target.value)}
      ></input>
    </form>
  );
};

export default LoginForm;
