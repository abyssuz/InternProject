import React from 'react';
import { Link, useRouteMatch, Switch, Route } from 'react-router-dom';
import ClientsList from '../ClientsList';
import CitiesList from '../CitiesList';
import OrdersList from '../OrdersList';
import RepairersList from '../RepairersList';

const AdminNav = () => {
  const { path, url } = useRouteMatch();

  return (
    <div>
      <nav className="admin-nav">
        <Link className="admin-nav-links" to={`${url}/clients`}>
          Clients
        </Link>
        <Link className="admin-nav-links" to={`${url}/cities`}>
          Cities
        </Link>
        <Link className="admin-nav-links" to={`${url}/orders`}>
          Orders
        </Link>
        <Link className="admin-nav-links" to={`${url}/repairers`}>
          Repairers
        </Link>
      </nav>
      
      <Switch>
        <Route path={`${path}/clients`}>
          <ClientsList />
        </Route>
        <Route path={`${path}/cities`}>
          <CitiesList />
        </Route>
        <Route path={`${path}/orders`}>
          <OrdersList />
        </Route>
        <Route path={`${path}/repairers`}>
          <RepairersList />
        </Route>
      </Switch>
    </div>
  );
};

export default AdminNav;
