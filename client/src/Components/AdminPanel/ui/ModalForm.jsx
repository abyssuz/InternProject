import React from 'react';
import SubmitButton from '../../commonUI/SubmitButton';
import TextInput from '../../commonUI/TextInput';

const ModalForm = ({ fields, fieldsValue, setFieldsValue, setVisible, add, remove }) => {
  return (
    <div className="modal-form">
      {fields.map((el, index) =>
        el != 'id' ? (
          <TextInput
            key={index}
            type="text"
            placeholder={el}
            value={fieldsValue[el]}
            onChange={(e) =>
              setFieldsValue({ ...fieldsValue, [el]: e.target.value })
            }
          ></TextInput>
        ) : (
          ''
        )
      )}
      <SubmitButton
        value="Save"
        onClick={() => {
          add(fieldsValue);
          fields.forEach((el) => setFieldsValue({ [el]: '' }));
          setVisible(false);
        }}
      />
      <SubmitButton
        value="Delete"
        onClick={() => {
          remove(fieldsValue.id);
          fields.forEach((el) => setFieldsValue({ [el]: '' }));
          setVisible(false);
        }}
      />
    </div>
  );
};

export default ModalForm;
