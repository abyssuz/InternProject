import React from 'react';

const AdminModal = ({ children, visible, setVisible }) => {
  const rootClasses = ['admin-modal'];
  
  if (visible) {
    rootClasses.push('active');
  }

  const onBackdropClick = () => {
    const clearedFields = {};
    Object.keys(children.props.fields).forEach((el) => clearedFields[el] = '');
    children.props.setFields(clearedFields);
    setVisible(false);
  };

  return (
    <div className={rootClasses.join(' ')} onClick={onBackdropClick}>
      <div className="admin-modal-content" onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>
  );
};

export default AdminModal;
