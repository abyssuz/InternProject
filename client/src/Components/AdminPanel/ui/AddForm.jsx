import React from 'react';
import SubmitButton from '../../commonUI/SubmitButton';
import TextInput from '../../commonUI/TextInput';

const AddForm = ({ fields, setFields, setVisible, add }) => {
  return (
    <div className="modal-form">
      {Object.keys(fields).map((el, index) =>
        el != 'id' ? (
          <TextInput
            key={index}
            type="text"
            placeholder={el}
            value={fields[el]}
            onChange={(e) =>
              setFields({ ...fields, [el]: e.target.value })
            }
          ></TextInput>
        ) : (
          ''
        )
      )}

      <SubmitButton
        value="Add"
        onClick={() => {
          add();
          Object.keys(fields).forEach((el) => setFields({ [el]: '' }));
          setVisible(false);
        }}
      />
    </div>
  );
};

export default AddForm;
