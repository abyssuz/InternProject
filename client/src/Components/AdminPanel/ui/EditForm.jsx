import React from 'react';
import SubmitButton from '../../commonUI/SubmitButton';
import TextInput from '../../commonUI/TextInput';

const EditModal = ({ fields, setFields, setVisible, edit, remove }) => {
  return (
    <div className="modal-form">
      {Object.keys(fields).map((el, index) =>
        el != 'id' ? (
          <TextInput
            key={index}
            type="text"
            placeholder={el}
            value={fields[el]}
            onChange={(e) => setFields({ ...fields, [el]: e.target.value })}
          ></TextInput>
        ) : (
          ''
        )
      )}

      <SubmitButton
        value="Save"
        onClick={() => {
          edit();
          Object.keys(fields).forEach((el) => setFields({ [el]: '' }));
          setVisible(false);
        }}
      />

      <SubmitButton
        value="Delete"
        onClick={() => {
          remove();
          Object.keys(fields).forEach((el) => setFields({ [el]: '' }));
          setVisible(false);
        }}
      />
    </div>
  );
};

export default EditModal;
