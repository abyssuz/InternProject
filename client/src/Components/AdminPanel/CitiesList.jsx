import React, { useState } from 'react';
import { CITY_URL } from '../../assets/apiUrls';
import SubmitButton from '../commonUI/SubmitButton';
import AdminModal from './ui/AdminModal';
import AddForm from './ui/AddForm';
import EditForm from './ui/EditForm';
import { useManageList } from '../../hooks/useManageList';

const CitiesList = () => {
  const [addModal, setAddModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [fields, setFields] = useState({ id: '', name: '' });

  const [list, addCity, editCity, removeCity] = useManageList(CITY_URL);

  const onListItemClick = (el) => {
    setFields(el);
    setEditModal(true);
  };

  const onAdd = () => {
    addCity(fields);
  };

  const onEdit = () => {
    editCity(fields);
  };

  const onRemove = () => {
    removeCity(fields.id);
  };

  return (
    <div>
      {list.map((el) => (
        <div
          className="list-item"
          key={el.id}
          onClick={() => onListItemClick(el)} >
          {el.name}
        </div>
      ))}

      <AdminModal visible={addModal} setVisible={setAddModal}>
        <AddForm
          fields={fields}
          setFields={setFields}
          setVisible={setAddModal}
          add={onAdd} />
      </AdminModal>

      <AdminModal visible={editModal} setVisible={setEditModal}>
        <EditForm
          fields={fields}
          setFields={setFields}
          setVisible={setEditModal}
          edit={onEdit}
          remove={onRemove} />
      </AdminModal>

      <SubmitButton value="Add city" onClick={() => setAddModal(true)} />
    </div>
  );
};

export default CitiesList;
