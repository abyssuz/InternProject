import React from 'react';
import './SubmitButton.css';

const SubmitButton = ({value, onClick}) => {
  return (
    <input
      className="submit-btn"
      type="submit"
      value={value}
      onClick={onClick}
    ></input>
  );
};

export default SubmitButton;
