import React from 'react';
import './TextInput.css';

const TextInput = ({ type, value, placeholder, onChange }) => {
  return (
    <input
      className="text-input"
      type={type}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
    ></input>
  );
};

export default TextInput;
