export const CLIENT_URL = "http://localhost:5000/api/client";
export const CITY_URL = "http://localhost:5000/api/city";
export const ORDER_URL = "http://localhost:5000/api/order";
export const REPAIRER_URL = "http://localhost:5000/api/repairer";
