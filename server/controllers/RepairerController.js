const { Op } = require('sequelize');
const { Repairer, City, RepairerCity } = require('../models/models');
const ApiError = require('../error/ApiError');

class RepairerController {
  async create(req, res) {
    const { name, rating, cities } = req.body;
    const repairer = await Repairer.create({ name, rating });

    cities.forEach(async (el) => {
      const city = await City.findOne({ where: { name: el } });
      if (city) {
        await RepairerCity.create({ cityId: city.id, repairerId: repairer.id });
      }
    });

    return res.json(repairer);
  }

  async edit(req, res, next) {
    const { id, name, rating } = req.body;
    const repairer = await Repairer.findOne({ where: { id } });

    if (repairer) {
      repairer.name = name;
      repairer.rating = rating;
      const editedRepairer = await repairer.save();
      return res.json(editedRepairer);
    } else {
      return next(ApiError.badRequest('Undefined ID!'));
    }
  }

  async delete(req, res, next) {
    const { id } = req.params;
    const repairer = await Repairer.findOne({ where: { id } });

    if (repairer) {
      await Repairer.destroy({ where: { id } });
      return res.json(`${repairer.name} deleted`);
    } else {
      return next(ApiError.badRequest('Undefined ID!'));
    }
  }

  async getByCity(req, res) {
    const { cityId } = req.params;
    let searchedRepairers = await RepairerCity.findAll({ where: { cityId } });

    searchedRepairers = searchedRepairers.map((el) => {
      return { id: el.repairerId };
    });

    const repairers = await Repairer.findAll({
      where: { [Op.or]: searchedRepairers },
    });

    return res.json(repairers);
  }

  async getAll(req, res) {
    const repairers = await Repairer.findAll();
    return res.json(repairers);
  }
}

module.exports = new RepairerController();
