const { Client } = require('../models/models');
const ApiError = require('../error/ApiError');

class ClientController {
  async delete(req, res) {}

  async getAll(req, res) {
    const clients = await Client.findAll();
    return res.json(clients);
  }
}

module.exports = new ClientController();
