const sequelize = require('../db');
const { City } = require('../models/models');
const ApiError = require('../error/ApiError');

class CityController {
  async create(req, res, next) {
    const { name } = req.body;

    try {
      const response = await sequelize.transaction(async (t) => {
        const city = await City.create({ name }, { transaction: t });
        return city;
      });
      return res.status(201).json(response);
    } catch (e) {
      return next(ApiError.internal(e));
    }
  }

  async delete(req, res, next) {
    const { id } = req.params;

    try {
      const response = await sequelize.transaction(async (t) => {
        const city = await City.findOne({ where: { id } }, { transaction: t });
        if (city) {
          await City.destroy({ where: { id } }, { transaction: t });
          return city.id;
        } else {
          throw new Error();
        }
      });

      return res.status(200).json(`City with id ${response} has been deleted`);
    } catch (e) {
      return next(ApiError.internal(e));
    }
  }

  async edit(req, res, next) {
    const { id } = req.params;
    const { name } = req.body;

    try {
      const response = await sequelize.transaction(async (t) => {
        const city = await City.findOne({ where: { id } }, { transaction: t });
        if (city) {
          city.name = name;
          const editedCity = await city.save({ transaction: t });
          return editedCity;
        } else {
          throw new Error();
        }
      });

      return res.status(201).json(response);
    } catch (e) {
      return next(ApiError.internal(e));
    }
  }

  async getAll(req, res) {
    const response = await sequelize.transaction(async (t) => {
      return await City.findAll({ transaction: t });
    });

    return res.status(200).json(response);
  }
}

module.exports = new CityController();
