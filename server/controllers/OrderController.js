const { Client, Order, City } = require('../models/models');
const ApiError = require('../error/ApiError');

class OrderController {
  async create(req, res, next) {
    try {
      const { name, email, clockSize, cityId, date, time, repairerId } = req.body;
      let client = await Client.findOne({ where: { email } });
      let city = await City.findOne({ where: { id: cityId } });

      if (!client) {
        client = await Client.create({ name, email });
      }

      const order = await Order.create({
        clockSize,
        city: city.name,
        date,
        time,
        clientId: client.id,
        repairerId,
      });

      return res.json(order);
    } catch (e) {
      next(ApiError.badRequest(e.message));
    }
  }

  async delete(req, res, next) {
    const { id } = req.params;
    const order = await Order.findOne({ where: { id } });

    if (order) {
      await Order.destroy({ where: { id } });
      return res.json(`Order with id ${order.id} deleted`);
    } else {
      return next(ApiError.badRequest('Undefined ID!'));
    }
  }

  async getAll(req, res) {
    const orders = await Order.findAll();
    return res.json(orders);
  }
}

module.exports = new OrderController();
