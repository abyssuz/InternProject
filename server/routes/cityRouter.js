const Router = require('express');
const CityController = require('../controllers/CityController');
const router = new Router();

router.post('/', CityController.create);
router.delete('/:id', CityController.delete);
router.put('/:id', CityController.edit);
router.get('/', CityController.getAll);

module.exports = router;
