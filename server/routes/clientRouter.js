const Router = require('express');
const ClientController = require('../controllers/ClientController');
const router = new Router();

router.get('/', ClientController.getAll);
router.delete('/', ClientController.delete);

module.exports = router;
