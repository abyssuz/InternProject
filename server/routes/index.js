const Router = require('express');
const router = new Router();
const clientRouter = require('./clientRouter')
const orderRouter = require('./orderRouter');
const cityRouter = require('./cityRouter');
const repairerRouter = require('./repairerRouter');

router.use('/client', clientRouter);
router.use('/order', orderRouter);
router.use('/city', cityRouter);
router.use('/repairer', repairerRouter);

module.exports = router;
