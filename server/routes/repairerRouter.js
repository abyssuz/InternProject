const Router = require('express');
const RepairerController = require('../controllers/RepairerController');
const router = new Router();

router.post('/', RepairerController.create);
router.put('/', RepairerController.edit);
router.delete('/:id', RepairerController.delete);
router.get('/:cityId', RepairerController.getByCity);
router.get('/', RepairerController.getAll);

module.exports = router;
