const Router = require('express');
const OrderController = require('../controllers/OrderController');
const router = new Router();

router.post('/', OrderController.create);
router.delete('/:id', OrderController.delete);
router.get('/', OrderController.getAll);

module.exports = router;
