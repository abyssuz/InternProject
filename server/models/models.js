const sequelize = require('../db');
const { DataTypes } = require('sequelize');

const Client = sequelize.define(
  'client',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: DataTypes.STRING, allowNull: false },
    email: { type: DataTypes.STRING, allowNull: false, unique: true },
  },
  { timestamps: false }
);

const City = sequelize.define(
  'city',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: DataTypes.STRING, allowNull: false },
  },
  { timestamps: false }
);

const Repairer = sequelize.define(
  'repairer',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: DataTypes.STRING, allowNull: false },
    rating: { type: DataTypes.INTEGER, allowNull: false },
  },
  { timestamps: false }
);

const Order = sequelize.define(
  'order',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    clockSize: { type: DataTypes.STRING, allowNull: false },
    city: { type: DataTypes.STRING, allowNull: false },
    date: { type: DataTypes.STRING, allowNull: false },
    time: { type: DataTypes.STRING, allowNull: false },
  },
  { timestamps: false }
);

const RepairerCity = sequelize.define(
  'repairer_city',
  {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  },
  { timestamps: false }
);

Client.hasMany(Order);
Order.belongsTo(Client);

Repairer.hasMany(Order);
Order.belongsTo(Repairer);

Repairer.belongsToMany(City, { through: RepairerCity });
City.belongsToMany(Repairer, { through: RepairerCity });
Repairer.hasMany(RepairerCity);
City.hasMany(RepairerCity);
RepairerCity.belongsTo(Repairer);
RepairerCity.belongsTo(City);

module.exports = {
  Client,
  City,
  Repairer,
  Order,
  RepairerCity,
};
